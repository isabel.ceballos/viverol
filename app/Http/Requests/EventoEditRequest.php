<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [    
         'title' =>'unique:eventos',        
            'end' => 'date|after:start',
        ];
    }
     public function attributes(){
         return [
            'start' => 'nombre del evento',
            'start' => 'Fecha de inicio',
            'end' => 'Fecha de fin',
        ];
    }
}
