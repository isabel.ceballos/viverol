<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tipo_Evento;
use App\Models\Comment;
use App\Models\Evento;
class HomeController extends Controller
{
	public function __construct(){
		$this->middleware('can:admin.index');
		$this->middleware('can:tipos.create');
		$this->middleware('can:tipos.store');
		$this->middleware('can:admin.comments');
		$this->middleware('can:comments.destroy');
		$this->middleware('can:admin.eventos');
		$this->middleware('can:eventos.destroy');
	}
	public function index(){
		return view('admin.index');
	}

	public function showUsers(){
		$usuarios=User::all();
		return view('admin.usuarios', compact('usuarios'));
	}

	public function showTipos(){
		$tipos=Tipo_Evento::all();
		return view('admin.tipos', compact('tipos'));
	}
	public function showComments(){
		$comments=Comment::all();
		return view('admin.comments', compact('comments'));
	}

	public function showEventos(){
		$eventos=Evento::all();
		return view('admin.eventos', compact('eventos'));
	}

	public function createTipo()
	{		
		return view('admin.createTipo');
	}

	public function storeTipo(Request $request)
	{
		$datos=$request->all();		
		$evento= Tipo_Evento::create($datos);       

		return redirect()->route("admin.tipos")->with('mensaje', 'Se ha creado el tipo de evento.');
	}

	public function destroyTipo(Tipo_Evento $tipo){
		$tipo->delete();
		return redirect()->route("admin.tipos")->with('mensaje', 'Eliminaste el tipo de evento.');
	}
	public function destroyComment(Comment $comment){
		$comment->delete();
		return redirect()->route("admin.comments")->with('mensaje', 'Eliminaste el comentario.');
	}

	public function destroyEvento(Evento $evento){
		$evento->delete();
		return redirect()->route("admin.eventos")->with('mensaje', 'Eliminaste el evento.');
	}

	public function editTipo(Tipo_Evento $tipo)

	{
		
		return view('admin.editTipo', ['tipo'=>$tipo]);
	}

	public function updateTipo(Request $request, Tipo_Evento $tipo)
	{
		$tipo->tipo=$request->tipo;

		$tipo->save();
		return redirect()->route("admin.tipos")->with('mensaje', 'Se ha editado la información del tipo de evento.');
	}

	public function destroyUser(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index')->with('mensaje', 'Se eliminó el usuario correctamente.');
    }
}
