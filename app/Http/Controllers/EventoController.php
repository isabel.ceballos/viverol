<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\Tipo_Evento;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\EventoCreateRequest;
use App\Http\Requests\EventoEditRequest;
use Barryvdh\DomPDF\Facade as PDF;
class EventoController extends Controller
{

public function index()    
{
  //$eventos=Evento::all();
  $eventos=Evento::simplePaginate(6);
  $tipos =Tipo_Evento::all();

  return view('eventos.index', compact('eventos', 'tipos'));
}


public function create()
{
  $tipos =Tipo_Evento::all();
 
  return view('eventos.create', compact('tipos'));
}

public function store(EventoCreateRequest $request)
{

  $user = auth()->user();
  $datos=$request->except(['_token','_method']);
  $datos['user_id']=$user->id;
  $datos['imagen']=$request->imagen->store('','eventos'); 
  $datos['slug'] =Str::slug($request->title);
  $evento= Evento::create($datos);       

  return redirect()->route("eventos.show", $evento->slug)->with('mensaje', 'Se ha creado el evento.');
}


public function show(Evento $evento)
{
  $tipos =Tipo_Evento::all();
  return view('eventos.show', ['evento'=>$evento, 'tipos'=>$tipos]);
}


public function edit(Evento $evento)

{
  $tipos =Tipo_Evento::all();
 
  return view('eventos.edit', ['evento'=>$evento, 'tipos'=>$tipos]);
}


public function update(EventoEditRequest $request, Evento $evento)
{
  $evento->title=$request->title;

  $evento->tipo_evento_id=$request->tipo_evento_id;  
  $evento->start=$request->start;
  $evento->end=$request->end;
  $evento->color=$request->color;
  $evento->descripcion=$request->descripcion;
  $evento->slug=Str::slug($request->title);
  if($request->hasFile('imagen') && $request->imagen->isValid()){
    Storage::disk('eventos')->delete($evento->imagen);
    $evento->imagen=$request->imagen->store('','eventos');
  }

  $evento->save();
  return redirect()->route("eventos.show", $evento)->with('mensaje', 'Se ha editado la información del evento.');
}


public function destroy(Evento $evento)
{
  $evento->delete();
  return redirect()->route("eventos.eventoUsuario")->with('mensaje', 'Eliminaste el evento.');
}

public function apuntar(Evento $evento){
  $user = auth()->user();
  $evento->usuariosApuntados()->attach($user->id);
  return redirect()->route("eventos.show", $evento)->with('mensaje', 'Te apuntaste al evento.');
}

public function desApuntar(Evento $evento){
  $user = auth()->user();
  $evento->usuariosApuntados()->detach($user->id);
  return redirect()->route("eventos.show", $evento)->with('mensaje', 'Te desapuntaste del evento.');
}

public function tipo (Tipo_Evento $tipo){
  $tipos= Tipo_Evento::all()->where('id', '=', $tipo->id);
  $eventos= Evento::where('tipo_evento_id', '=', $tipo->id)->simplePaginate(6);

  return view('eventos.tipo', compact ('eventos', 'tipos'));
}

public function eventoUsuario()
{
  $user = auth()->user();
  $user_id= $user->id;
  $eventos=Evento::where('user_id', '=', $user_id)->simplePaginate(6);
  return view('eventos.eventoUsuario', compact('eventos', 'user'));
}

public function agenda()
{
 $eventos=Evento::all();
 $tipos =Tipo_Evento::all();
 
 return view('agendas.index', compact('tipos', 'eventos'));
}
public function ver(){

 $data['eventos']=Evento::all();
 return response()->json($data['eventos']);
}

public function buscar(Request $request){
        
        $busqueda=$request->busqueda;
        $evento=Evento::where('title', 'like', "%$busqueda%")->pluck('title');
        return response()->json($evento);
    }

public function pdf(Evento $evento){
  
  $pdf =PDF::loadView('eventos.apuntados',  ['evento'=>$evento]);
  $titulo= $evento->title;
  $nombre= $titulo. "-usuarios.pdf";
  return $pdf->download($nombre);
}

}
