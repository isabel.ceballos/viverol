<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function index()
    {
        return view('admin.users.index');
    }
    
   
    
    public function edit(User $user)

    {
        $roles=Role::all();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->roles);
        return redirect()->route('admin.users.edit', $user)->with('info', 'Se asignaron los roles correctamente.');
    }

    public function editProfile(User $user)

    {
      return view('profile.show', ['user'=>$user]);
  }


  public function updateProfile(Request $request, User $user)
  {
      $user->name=$request->name;
      $user->email=$request->email;
      $user->password=auth()->user()->password;
      if($request->hasFile('imagen') && $request->imagen->isValid()){
        Storage::disk('users')->delete($user->profile_photo_path);
        $user->profile_photo_path=$request->imagen->store('','users');
    }

    $user->save();
    return redirect()->route("profile.show", $user)->with('mensaje', 'Se ha editado la información del usuario.');
}



}
