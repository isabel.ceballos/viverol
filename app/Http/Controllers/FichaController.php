<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ficha;

class FichaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $user_id= $user->id;
        $fichas=Ficha::all()->where('user_id', '=', $user_id);
        return view('fichas.index', compact('fichas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fichas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $datos=$request->all();
        $datos['user_id']=$user->id;
        $datos['imagen']=$request->imagen->store('','fichas');
        $ficha= Ficha::create($datos);       

        return redirect()->route("fichas.show", $ficha->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ficha $ficha)
    {
        return view('fichas.show', ['ficha'=>$ficha]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ficha $ficha)
    {
        return view('fichas.edit', ['ficha'=>$ficha]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ficha $ficha)
    {
        $ficha->nombre_personaje=$request->nombre_personaje;
        $ficha->tipo_personaje=$request->tipo_personaje;
        $ficha->descripcion=$request->descripcion;
        if($request->hasFile('imagen') && $request->imagen->isValid()){
            Storage::disk('fichas')->delete($ficha->imagen);
            $ficha->imagen=$request->imagen->store('','fichas');
        }

        $ficha->save();
        return redirect()->route("fichas.show", $ficha)->with('mensaje', 'Se ha editado la información de la ficha.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ficha $ficha)
    {
        $ficha->delete();
        return redirect()->route("fichas.index")->with('mensaje', 'Se ha eliminado la ficha.');
    }
}
