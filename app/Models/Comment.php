<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Evento;

class Comment extends Model
{
    use HasFactory;

    protected $table='comments';
    protected $fillable = [
        'content', 'parent_id', 'post_id', 'user_id',
    ];

    public function evento() 
    {
        return $this->belongsTo(Evento::class);
    }
 
    public function user() 
    {
        return $this->belongsTo(User::class);
    }
 
    public function parent() 
    {
        return $this->belongsTo('App\Models\Comment', 'parent_id');
    }
 
    public function replies() 
    {
        return $this->hasMany('App\Models\Comment', 'parent_id');
    }

}
