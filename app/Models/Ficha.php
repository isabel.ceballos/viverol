<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Ficha extends Model
{
    use HasFactory;
    protected $table= 'fichas';
    	protected $guarded=[];
    public function usuarios(){
    	return $this->belongsTo(User::class);
    }
}
