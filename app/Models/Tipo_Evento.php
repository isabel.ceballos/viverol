<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Evento;

class Tipo_Evento extends Model
{
    use HasFactory;
    protected $table= 'tipo_eventos';
    protected $guarded=[];
    public function eventosContiene(){
        return $this->hasMany(Evento::class);
    }
}
