<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Tipo_Evento;
use App\Models\Comment;
use Carbon\Carbon;

class Evento extends Model
{
	use HasFactory;
	protected $table= 'eventos';
	protected $guarded=[];

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function usuariosApuntados() {
		return $this->belongsToMany(User::class);
	}

	public function tipoEvento(){
		return $this->belongsTo(Tipo_Evento::class);
	}

	public function comments() 
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }

	public function getCreated(){
		$date = Carbon::parse($this->created_at);
		$now = Carbon::now();
		$diff = $date->diffInDays($now);
		return $diff;
	}

	public function getEstado(){
		$date=Carbon::parse($this->end);
		$now = Carbon::now();
		if($date<$now){
			return false;
		}else{
			return true;
		}

	}
	public function getRouteKeyName(){
        return 'slug';
    }
	
}
