<form action="{{route('comments.store', $evento)}}" method="POST">
	{{ csrf_field() }}
	
	@if (isset($comment->id))
		<input type="hidden" name="parent_id" value="{{$comment->id}}">
	@endif
	
	<input type="hidden" name="user_id" value="{{\auth()->id()}}">
 	<h5>Escribe tu comentario:</h5>
 	<div class="form-group">		
		<textarea name="content" cols="1" rows="1" style="width:500px; height:200px;"></textarea>
	</div>
  	<button type="submit" class="btn btn-primary">Enviar</button>
</form>
