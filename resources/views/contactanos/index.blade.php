@extends('layouts.master')
@section('titulo')Contáctanos
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif
<form action="{{ route('contactanos.store') }}" method="post" enctype="multipart/form-data">
	@csrf	
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">		
			<div class="card-header text-center">
				<h2 class="card-title">Formulario de contacto</h2>
			</div>
			<div class="form-group">		
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required>    
			</div>
			@error('nombre')
			<p><strong>{{ $message }}</strong></p>
			@enderror
			<div class="form-group">		
				<label for="email">Correo</label>
				<input type="email" name="email" id="email" class="form-control" required>    
			</div>
			@error('email')
			<p><strong>{{ $message }}</strong></p>
			@enderror
			
			<div class="form-group">
				<label for="mensaje">Mensaje</label>
				<textarea name="mensaje" id="mensaje" class="form-control" rows="3"></textarea>
			</div>	
			@error('mensaje')
			<p><strong>{{ $message }}</strong></p>
			@enderror		
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Enviar</button>
			</div>
		</div>
	</div>
</form>
@endsection