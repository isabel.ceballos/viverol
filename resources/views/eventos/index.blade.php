@extends('layouts.master')
@section('titulo')Index
@endsection
@section('content')
<section id="gallery">
	<div class="container">
		<div class="row">
			@foreach($eventos as $clave=> $evento)
			<div class="col-lg-4 mb-4">
				<div class="card"><a href="{{ route('eventos.show' , $evento) }}">
					<img src="{{asset('assets/imagenes/')}}/{{$evento->imagen}}" alt="" class="card-img-top">
				</a>
				<div class="card-body">
					<a href="{{ route('eventos.show' , $evento) }}">
						<h5 class="card-title">{{$evento->title}}</h5>
					</a>
					<p><strong>Tipo de evento: </strong>{{$evento->tipoEvento->tipo}}</p>
					<ul class="list-group list-group-flush">			
						@if($evento->getEstado())
						<li class="list-group-item">Evento pendiente.</li>
						@else
						<li class="list-group-item">Evento terminado.</li>	
						@endif
						<li class="list-group-item">Evento creado hace {{$evento->getCreated()}} días.</li>
					</ul>
					<br>
					<a href="{{route('eventos.show' , $evento)}}" class="btn btn-outline-warning">Leer más</a>					
				</div>
			</div>
		</div>
		@endforeach    
	</div>
</div>
<center>{{ $eventos->links()}}</center>
</section>
@endsection