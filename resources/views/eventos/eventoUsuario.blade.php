@extends('layouts.master')
@section('titulo')Eventos del usuario
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-danger">
	{{session('mensaje')}}
</div>
@endif
<section id="gallery">
	<div class="container">
		<div class="row">
			<h1>TUS EVENTOS CREADOS</h1>
			@foreach($eventos as $clave=> $evento)
			<div class="col-lg-4 mb-4">
				<div class="card"><a href="{{ route('eventos.show' , $evento) }}">
					<img src="{{asset('assets/imagenes/')}}/{{$evento->imagen}}" alt="" class="card-img-top">
				</a>
				<div class="card-body">
					<a href="{{ route('eventos.show' , $evento) }}">
						<h5 class="card-title">{{$evento->title}}</h5>
					</a>
					<p><strong>Tipo de evento: </strong>{{$evento->tipoEvento->tipo}}</p>
					<ul class="list-group list-group-flush">						
						<li class="list-group-item">Evento creado hace {{$evento->getCreated()}} días.</li>
					</ul>
					<br>
					<a href="{{route('eventos.show' , $evento)}}" class="btn btn-outline-warning">Leer más</a>					
				</div>							
			</div>
		</div>
		@endforeach    
	</div>
			<div class="row">
			<h1>EVENTOS A LOS QUE TE APUNTASTE</h1>
			@foreach($user->eventosApuntados as $evento)
			<div class="col-lg-4 mb-4">
				<div class="card"><a href="{{ route('eventos.show' , $evento) }}">
					<img src="{{asset('assets/imagenes/')}}/{{$evento->imagen}}" alt="" class="card-img-top">
				</a>
				<div class="card-body">
					<a href="{{ route('eventos.show' , $evento) }}">
						<h5 class="card-title">{{$evento->title}}</h5>
					</a>
					<p><strong>Tipo de evento: </strong>{{$evento->tipoEvento->tipo}}</p>
					<ul class="list-group list-group-flush">												
						<li class="list-group-item">Evento creado hace {{$evento->getCreated()}} días.</li>
					</ul>
					<br>
					<a href="{{route('eventos.show' , $evento)}}" class="btn btn-outline-warning">Leer más</a>					
				</div>							
			</div>
		</div>
		@endforeach    
	</div>
</div>
<center>{{ $eventos->links()}}</center>
</section>

@endsection