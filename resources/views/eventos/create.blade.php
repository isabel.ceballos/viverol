@extends('layouts.master')
@section('titulo')Crear
@endsection
@section('content')
<form action="{{ route('eventos.store') }}" method="post" enctype="multipart/form-data">
	@csrf
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">		
			<div class="card-header text-center">
				<h2 class="card-title">Añadir evento</h2>
			</div>
			<div class="form-group">		
				<label for="title">Nombre del evento</label>
				<input type="text" name="title" id="title" class="form-control" required>    
			</div>
			@error('title')
			<p><strong>{{ $message }}</strong></p>
			@enderror
			<div class="form-group">
				<label for="tipo_evento_id">Tipo de evento</label>
			<select name="tipo_evento_id" id="tipo_evento_id" class="form-control">
				@foreach($tipos as $tipo)
				<option value="{{ $tipo->id }}">{{ $tipo->tipo }}</option>
				@endforeach
			</select>
			</div>			
			<div class="form-group">
				<label for="start">Fecha de inicio</label>
				<input type="datetime-local" name="start" id="start" class="form-control" required>
			</div>
			@error('start')
			<p><strong>{{ $message }}</strong></p>
			@enderror	
			<div class="form-group">
				<label for="end">Fecha de fin</label>
				<input type="datetime-local" name="end" id="end" class="form-control" required>
			</div>
			@error('end')
			<p><strong>{{ $message }}</strong></p>
			@enderror		
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>
			</div>
			<div class="form-group">
				<label for="color">Color</label>
				<input type="color" name="color" id="color" class="form-control">
			</div>
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" name="imagen" id="imagen" class="form-control" required>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir evento</button>
			</div>
		</div>
	</div>
</form>
@endsection