@extends('layouts.master')
@section('titulo')Editar
@endsection
@section('content')
<form action="{{ route('eventos.update', $evento) }}" method="post" enctype="multipart/form-data">
	@csrf
	@method ('put')
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">		
			<div class="card-header text-center">
				<h2 class="card-title">Editar evento</h2>
			</div>
			<div class="form-group">		
				<label for="title">Nombre del evento</label>
				<input type="text" name="title" value="{{$evento->title}}" id="title" class="form-control">   
			</div>	
			@error('title')
			<p><strong>{{ $message }}</strong></p>
			@enderror
			<div class="form-group">
				<label for="tipo_evento_id">Tipo de evento</label>
			<select name="tipo_evento_id" id="tipo_evento_id" class="form-control">
				@foreach($tipos as $tipo)
				<option value="{{ $tipo->id }}">{{ $tipo->tipo }}</option>
				@endforeach
			</select>
			</div>									
			<div class="form-group">
				<label for="start">Fecha de inicio</label>
				<input type="datetime" name="start" value="{{$evento->start}}" id="start" class="form-control">
			</div>	
			@error('start')
			<p><strong>{{ $message }}</strong></p>
			@enderror
			<div class="form-group">
				<label for="end">Fecha de fin</label>
				<input type="datetime" name="end" value="{{$evento->end}}" id="end" class="form-control">
			</div>
			@error('end')
			<p><strong>{{ $message }}</strong></p>
			@enderror		
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{$evento->descripcion}}</textarea>
			</div>
			<div class="form-group">
				<label for="color">Color</label>
				<input type="color" name="color" id="color" class="form-control">
			</div>
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" name="imagen" id="imagen" class="form-control">
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar evento</button>
			</div>
		</div>
	</div>
</form>
@endsection