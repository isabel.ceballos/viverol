@extends('layouts.master')
@section('titulo')Show
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif

<div class="d-flex justify-content-center">

	<div class="card" style="width: 50rem;">
		<img class="img-fluid" src="{{asset('assets/imagenes/')}}/{{$evento->imagen}}" alt="Card image cap" style="width: 30rem;">
		<div class="card-body">
			<h2 class="card-title">{{$evento->title}}</h2>			
			<p><strong>Tipo de evento: </strong>{{$evento->tipoEvento->tipo}}</p>

			<h5 class="card-title">Descripción:</h5>		

			@php 
			$parrafos = explode(PHP_EOL, $evento->descripcion);
			@endphp

			@foreach($parrafos as $parrafo)
			<p>{{ $parrafo }}</p>
			@endforeach

			<h5 class="card-title">Fecha inicio:</h5>
			<p>{{$evento->start}}</p>
			<h5 class="card-title">Fecha fin:</h5>
			<p>{{$evento->end}}</p>
			<p><strong>Creador del evento: </strong>{{$evento->user->name}}</p>
			<p> <strong>Contacto: </strong>{{$evento->user->email}}</p>

			@if (isset(auth()->user()->id))
			@if (auth()->user()->id == $evento->user_id)
			<form action="{{ route('eventos.destroy', $evento) }}" method="post">
				@csrf
				@method('delete')
				<a href="{{route('eventos.edit', $evento) }}" class="btn btn-dark">Editar</a>
				<button type= "submit" class="btn btn-danger">Eliminar</button>
				<a href="{{route('eventos.eventoUsuario') }}" class="btn btn-light">Volver al listado</a>
			</form>
			@endif	
			@endif
			@if (isset(auth()->user()->eventosApuntados))
			@php		
			foreach (auth()->user()->eventosApuntados as $eventoApuntado){
				$ids[]=$eventoApuntado->id;
			}			
			@endphp
			@if($evento->getEstado())
			@if(isset($ids))			
			@if (isset(auth()->user()->id))			
			@if (auth()->user()->id != $evento->user_id && !in_array($evento->id, $ids))
			<form action="{{route('eventos.apuntar', $evento) }}" method="post">
				@csrf
				<button type="submit" class="btn btn-dark">Apuntarse	
				</button>
			</form>
			@elseif(auth()->user()->id != $evento->user_id && in_array($evento->id, $ids))
			<form action="{{route('eventos.desapuntar', $evento) }}" method="post">
				@csrf
				<button type="submit" class="btn btn-danger">Desapuntarse	
				</button>
			</form>
			@endif
			@endif
			@elseif(auth()->user()->id != $evento->user_id)
			<form action="{{route('eventos.apuntar', $evento) }}" method="post">
				@csrf
				<button type="submit" class="btn btn-dark">Apuntarse	
				</button>
			</form>
			@endif
			@endif
			@endif


		</div>
	</div>
</div>
@if (isset(auth()->user()->id))
@if (auth()->user()->id == $evento->user_id)
<div class="d-flex justify-content-center">
	<div class="card" style="width: 50rem;">
		<div class="card-body">			
			<h2 class="card-title">USUARIOS APUNTADOS AL EVENTO</h2>
			@foreach($evento->usuariosApuntados as $usuario)
			<p class="card-text"><strong>Usuario:</strong> {{ $usuario->name }} <strong>Email: </strong>{{ $usuario->email }}</p>
			@endforeach
			<p>
				<a href="{{ route('usuarios.pdf', $evento) }}" class="btn btn-sm btn-primary">
					Descargar usuarios apuntados en PDF
				</a>
			</p>
		</div>

	</div>
</div>
@endif	
@endif
<div class="d-flex justify-content-center">
	<div class="card" style="width: 50rem;">
		<div class="card-body">
			<h2 class="card-title">Comentarios:</h2>
			@include('comments.list', ['comments' => $evento->comments])            
			@if (isset(auth()->user()->id))
			@include('comments.form')
			@endif
		</div>
	</div>
</div>
@endsection
