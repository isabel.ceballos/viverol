<div>
	<div class="card">
		
		@if($users->count())
		<div class="card-body">
			<table class="table table-striped">
				<thead class="table-warning">
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Nombre</th>
						<th scope="col">Email</th>
						<th scope="col">Fecha de creación</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $clave=> $user)
					<tr>
						<th scope="row">{{ $user->id}}</th>
						<td>{{ $user->name}}</td>
						<td>{{ $user->email}}</td>
						<td>{{ $user->created_at}}</td>
						<td><form action="{{ route('users.destroy', $user) }}" method="post">	
							@csrf
							@method('delete')			
							<a href="{{ route('admin.users.edit', $user) }}" class="btn btn-dark">Editar</a>
							<button type= "submit" class="btn btn-danger">Eliminar</button>
						</form>
					</td>		
				</tr> 
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="class-footer">
		{{ $users->links() }}
	</div>
	@else
	<div class="card-body">
		<strong>No hay registros</strong>
	</div>
	@endif
</div>
</div>
