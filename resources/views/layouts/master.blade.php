<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  

  <!-- Bootstrap CSS -->
  <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
 
  <link href="{{ url('/assets/sidebar/css/style.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  
  <link href="" rel="stylesheet"> 
  <link href="{{url('/assets/js/jquery-ui.css')}}" rel="stylesheet">
  <script src="{{url('/assets/js/jquery-3.5.1.min.js')}}"></script>
  <script src="{{url('/assets/js/jquery-ui.js')}}"></script>
 
  
  <style>
    .card-img-top {
      width: 100%;
      height: 10vw;
      object-fit: cover;
    }
  </style>

  <title>@yield('titulo')</title>
  @yield('css')
  @yield('scripts')
  
</head>
<body>

  @include('layouts.partials.navbar')  

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>  
  <script src="{{ url('/assets/sidebar/js/main.js') }}"></script>
  <script src="{{ url('/assets/sidebar/js/bootstrap.min.js') }}"></script>


  


  <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  -->
  
</body>
</html>
