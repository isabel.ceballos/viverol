@php
$tipos=\App\Models\Tipo_Evento::all();
@endphp
<div class="wrapper d-flex align-items-stretch">
  <nav id="sidebar"> 
    <div class="p-4 pt-5">
      <a href="{{url('/')}}" class="img logo rounded-circle mb-5" style="background-image: url('{{ asset('assets/imagenes/viverol.jpg') }}');"></a>
      <ul class="list-unstyled components mb-5">
        <li class="nav-item">
          <a href="{{url('/')}}" class="nav-link">VIVEROL</a>
        </li>
        @if(Auth::check() )
        <li class="active">
          <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{ Auth::user()->name }}</a>
          <ul class="collapse list-unstyled" id="homeSubmenu">
            <li>
              <a href="{{ route('profile.show') }}" >Ver perfil</a>
            </li>
            @can('admin.index')
            <li>
              <a href="{{ route('admin.index') }}" >Administración</a>
            </li>
            @endcan
            <li>
              <a href="{{route('fichas.index')}}" class="nav-link {{ request()->routeIs('fichas.*') && !request()->routeIs('fichas.create')? ' active' : ''}}">Tus fichas</a>
            </li>
            <li>
              <a href="{{route('fichas.create')}}">Nueva ficha de personaje</a>
            </li>
            <li>
              <a href="{{route('eventos.eventoUsuario')}}">Tus eventos</a>
            </li>
            <li class="nav-item">
              <a href="{{ route('logout') }}"  class="nav-link"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" >
              <span class="glyphicon glyphicon-off"></span>
              Cerrar sesión
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
        </ul>
      </li>      

      @else
      <ul class="navbar-nav navbar-right">
        <li class="nav-item">
          <a href="{{url('login')}}" class="nav-link">Login</a>
        </li>
      </ul>
      <ul class="navbar-nav navbar-right">
        <li class="nav-item">
          <a href="{{url('register')}}" class="nav-link">Registro</a>
        </li>
      </ul>
      @endif 
      <li class="active">
        <a href="#tipoSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Tipos de evento</a>
        <ul class="collapse list-unstyled" id="tipoSubmenu">   
          @foreach($tipos as $tipo)         
          <li>
            <a href="{{route('eventos.tipo', $tipo)}}" >{{$tipo->tipo}}</a>
          </li>
          @endforeach            
        </ul>
      </li> 
    </ul>
  </div>
</nav>

<!-- Page Content  -->
<div id="content" class="p-4 p-md-5">

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">

      <button type="button" id="sidebarCollapse" class="btn btn-primary">
        <i class="fa fa-bars"></i>
        <span class="sr-only">Toggle Menu</span>
      </button>
      <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link {{ request()->routeIs('/')? ' active' : ''}}" href="{{url('/')}}">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('eventos.create')? ' active' : ''}}" href="{{route('eventos.create')}}">Nuevo evento</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('agendas.index')? ' active' : ''}}" href="{{route('agendas.index')}}">Agenda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('contactanos.index')? ' active' : ''}}" href="{{ route('contactanos.index') }}">Contáctanos</a>
          </li>
          <form class="d-flex">
            <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
            <button type="button" class="btn btn-outline-warning">Buscar</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>
  @yield('content')    
  
</div>
</div>


<script>
  function generateSlug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

  $(document).ready(function(){
    $( "#busqueda" ).autocomplete({
      source: function( query, result ) {
        $.ajax( {
          type:"POST",
          url: "{{url('eventos/buscar')}}",
          dataType: "json",
          data: {
            "_token": "{{csrf_token()}}",
            "busqueda": query['term'],
          },
          success: function( data ) {
            result( data );
          }
        });
      },
      position: {
        my: "left+0 top+8", //Mover 8px abajo.
      },
      select: function (event,ui){
        window.location=
        window.location.origin+"/eventos/"+generateSlug(ui.item.value);
      }
      });
  });

</script>









