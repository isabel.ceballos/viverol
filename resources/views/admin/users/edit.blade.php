@extends('adminlte::page')
@section('title', 'Admin')

@section('content_header')
<h1>Asignar un rol</h1>

@stop

@section('content')
@if(session('info'))
<div class="alert alert-success">
	{{session('info')}}
</div>
@endif
<div class="card">
	<div class="card-body">
		<p class="h5">Nombre:</p>
		<p class="form-control">{{ $user->name }}</p>
		<h2 class="h5">Lista de roles</h2>
		{!! Form::model($user, ['route' => ['admin.users.update', $user], 'method' =>'put']) !!}
		@foreach($roles as $role)
		<div>
			<label>
				{!!  Form::checkbox('roles[]', $role->id, false, ['class'=> 'mr-1']) !!}
				{{ $role->name }}
			</label>
		</div>
		@endforeach
		{!! Form::submit('Asignar rol', ['class'=>'btn btn-success mt-2']) !!}
		{!! Form::close() !!}
	</div>
</div>

@stop

@section('css')

@stop

@section('js')

<script>

</script>
@stop