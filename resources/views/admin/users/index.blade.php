@extends('adminlte::page')
@section('title', 'Admin')

@section('content_header')
<h1>Lista de usuarios</h1>

@stop

@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif
@livewire('admin.users-index')
@stop

@section('css')

@stop

@section('js')
 
<script>

</script>
@stop