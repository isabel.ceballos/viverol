@extends('adminlte::page')
@section('title', 'Comentarios')

@section('content_header')
<h1>Comentarios</h1>

@stop

@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif

<table class="table table-bordered">
	<thead class="table-warning">
		<tr>
			<th scope="col">Comentario</th>
			<th scope="col">Evento</th>
			<th scope="col">Nombre del usuario</th>
			<th scope="col">Fecha de creación</th>
		</tr>
	</thead>
	<tbody>
		@foreach($comments as $clave=> $comment)

		<tr>
			<th scope="row">{{$comment->content}}</th>
			<td>{{ $comment->evento->title }}</td>
			<td>{{ $comment->user->name }}</td>
			<td>{{ $comment->user->created_at }}</td>
			<td><form action="{{ route('comments.destroy', $comment) }}" method="post">
				@csrf
				@method('delete')			
				
				<button type= "submit" class="btn btn-danger">Eliminar</button>
			</td>
		</form>
	</tr> 
	@endforeach  
</tbody>
</table>



@stop

@section('css')

@stop

@section('js')
<script>

</script>
@stop
