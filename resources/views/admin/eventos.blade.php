@extends('adminlte::page')
@section('title', 'Comentarios')

@section('content_header')
<h1>Comentarios</h1>

@stop

@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif

<table class="table table-bordered">
	<thead class="table-warning">
		<tr>
			<th scope="col">Nombre del evento</th>
			<th scope="col">Creador</th>
			<th scope="col">Fecha de creación</th>
		</tr>
	</thead>
	<tbody>
		@foreach($eventos as $clave=> $evento)

		<tr>
			<th scope="row">{{$evento->title}}</th>
			<td>{{$evento->user->name}}</td>
			<td>{{$evento->user->created_at}}</td>
			<td><form action="{{ route('eventos.destroyEvento', $evento) }}" method="post">
				@csrf
				@method('delete')			
				
				<button type= "submit" class="btn btn-danger">Eliminar</button>
			</td>
		</form>
	</tr> 
	@endforeach  
</tbody>
</table>



@stop

@section('css')

@stop

@section('js')
<script>

</script>
@stop
