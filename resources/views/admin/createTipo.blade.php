@extends('adminlte::page')
@section('title', 'Tipos')

@section('content_header')
<h1>Crear tipo de evento</h1>

@stop

@section('content')

<form action="{{ route('tipos.store') }}" method="post" enctype="multipart/form-data">
	@csrf
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">
			<div class="form-group">		
				<label for="tipo">Nombre del tipo de evento</label>
				<input type="text" name="tipo" id="tipo" class="form-control" required>   
			</div>			
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Crear</button>
			</div>
		</div>
	</div>
</form>  
</tbody>
</table>


@stop

@section('css')

@stop

@section('js')
<script>

</script>
@stop
