@extends('adminlte::page')
@section('title', 'Tipos')

@section('content_header')
<h1>Editar tipo de evento</h1>

@stop

@section('content')

<form action="{{ route('tipos.update', $tipo) }}" method="post" enctype="multipart/form-data">
	@csrf
	@method ('put')
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">
			<div class="form-group">		
				<label for="tipo">Nombre del tipo de evento</label>
				<input type="text" name="tipo" value="{{$tipo->tipo}}" id="tipo" class="form-control">   
			</div>			
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar</button>
			</div>
		</div>
	</div>
</form>  
</tbody>
</table>


@stop

@section('css')

@stop

@section('js')
<script>

</script>
@stop
