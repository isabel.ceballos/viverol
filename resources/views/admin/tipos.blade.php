@extends('adminlte::page')
@section('title', 'Tipos')

@section('content_header')
<h1>Tipos de evento</h1>

@stop

@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif

<table class="table table-bordered">
	<thead class="table-warning">
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Nombre</th>
			<th scope="col">Fecha de creación</th>
		</tr>
	</thead>
	<tbody>
		@foreach($tipos as $clave=> $tipo)

		<tr>
			<th scope="row">{{$tipo->id}}</th>
			<td>{{$tipo->tipo}}</td>
			<td>{{$tipo->created_at}}</td>
			<td><form action="{{ route('tipos.destroy', $tipo) }}" method="post">
				@csrf
				@method('delete')				
				<a href="{{route('admin.editTipo', $tipo) }}" class="btn btn-dark">Editar</a>
				<button type= "submit" class="btn btn-danger">Eliminar</button>
			</td>
		</form>
	</tr> 
	@endforeach  
</tbody>
</table>
<a href="{{route('tipos.create')}}" class="btn btn-success">Añadir tipo</a>


@stop

@section('css')

@stop

@section('js')
<script>

</script>
@stop
