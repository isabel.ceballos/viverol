@extends('layouts.master')
@section('titulo')Editar
@endsection
@section('content')
<form action="{{ route('fichas.update', $ficha) }}" method="post" enctype="multipart/form-data">
	@csrf
	@method ('put')
	<div class="offset-md-3 col-md-6">
		<div class="card" style="width: 50rem;">		
			<div class="card-header text-center">
				<h2 class="card-title">Editar ficha de personaje</h2>
			</div>
			<div class="form-group">		
				<label for="nombre_personaje">Nombre del personaje</label>
				<input type="text" name="nombre_personaje" value="{{$ficha->nombre_personaje}}" id="nombre_personaje" class="form-control" required>    
			</div>
			<div class="form-group">		
				<label for="tipo_personaje">Tipo de personaje</label>
				<input type="text" name="tipo_personaje" value="{{$ficha->tipo_personaje}}" id="tipo_personaje" class="form-control" required>    
			</div>	
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<textarea name="descripcion" id="descripcion"class="form-control" rows="3">{{$ficha->descripcion}}</textarea>
			</div>
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" name="imagen" id="imagen" class="form-control" required>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar ficha</button>
			</div>
		</div>
	</div>
</form>
@endsection