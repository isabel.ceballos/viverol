@extends('layouts.master')
@section('titulo')Index
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-danger">
	{{session('mensaje')}}
</div>
@endif
<h2 class="card-title">TUS FICHAS DE PERSONAJE</h2>
<div class="card text-center">
	@foreach($fichas as $clave=> $ficha)
  <div class="card-header">
    
  </div>
  <div class="card-body">    
    <a href="{{ route('fichas.show' , $ficha) }}">
				<img src="{{asset('assets/imagenes/')}}/{{$ficha->imagen}}" class="img-fluid" alt="Responsive image" style="height:200px"/>
				<div class="card-body">
					<h5 class="card-title">{{$ficha->nombre_personaje}}</h5>
				</div>
			</a>
			   
  </div>  
  @endforeach
</div>

@endsection