@extends('layouts.master')
@section('titulo')Show
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif
<div class="d-flex justify-content-center">
	<div class="card" style="width: 50rem;">
		<img class="card-img-top img-fluid rounded-pill" src="{{asset('assets/imagenes/')}}/{{$ficha->imagen}}" alt="Card image cap" style="width: 30rem;">
		<div class="card-body">
			<h2 class="card-title">{{$ficha->nombre_personaje}}</h2>			
			<h5 class="card-title">Descripción:</h5>

			@php 
			$parrafos = explode(PHP_EOL, $ficha->descripcion);
			@endphp

			@foreach($parrafos as $parrafo)
			<p>{{ $parrafo }}</p>
			@endforeach
			
			<h5 class="card-title">Tipo de personaje:</h5>
			<p>{{$ficha->tipo_personaje}}</p>
			@if (isset(auth()->user()->id))
			@if (auth()->user()->id == $ficha->user_id)
			<form action="{{ route('fichas.destroy', $ficha) }}" method="post">
				@csrf
				@method('delete')
				<a href="{{route('fichas.edit', $ficha) }}" class="btn btn-dark">Editar</a>
				<button type= "submit" class="btn btn-danger">Eliminar</button>
				<a href="{{route('fichas.index') }}" class="btn btn-light">Volver al listado</a>
			</form>
			@endif
			@endif

			
		</div>
	</div>
</div>
@endsection