@extends('layouts.master')
@section('titulo')Perfil
@endsection
@section('content')
@if (session('mensaje'))
<div class="alert alert-success">
    {{session('mensaje')}}
</div>
@endif

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Perfil') }} de {{ auth()->user()->name }}
        </h2>
    </x-slot>
<form action="{{ route('users.update', $user) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method ('put')
    <div>
        <img class="img-fluid" src="{{asset('assets/imagenes/')}}/{{auth()->user()->profile_photo_path}}" style="width: 20rem;">
        <div class="form-group">        
            <label for="name">Nombre del usuario</label>
            <input type="text" name="name" value="{{ auth()->user()->name }}" id="name" class="form-control">   
        </div> 
        <div class="form-group">        
            <label for="title">Email del usuario</label>
            <input type="email" name="email" value="{{ auth()->user()->email }}" id="email" class="form-control">   
        </div>   
        <div class="form-group">
            <label for="imagen">Imagen</label>
            <input type="file" name="imagen" id="imagen" class="form-control">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar perfil</button>
        </div>
        
    </div>
</form>
</x-app-layout>
@endsection
