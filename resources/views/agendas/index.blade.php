@extends('layouts.master')
@section('scripts')
<link rel="stylesheet" href="{{ asset('fullcalendar/core/main.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/daygrid/main.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/list/main.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/timegrid/main.css') }}">

<script src="{{ asset('fullcalendar/core/main.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction/main.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid/main.js') }}"></script>
<script src="{{ asset('fullcalendar/list/main.js') }}"></script>
<script src="{{ asset('fullcalendar/timegrid/main.js') }}"></script>

<script>
//CALENDARIO
document.addEventListener('DOMContentLoaded', function() {
	var calendarEl = document.getElementById('calendar');

	var calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: [ 'dayGrid', 'interaction', 'timeGrid', 'list' ],
		header:{
			left:'prev, next today Miboton',
			center:'title',
			right: 'dayGridMonth, timeGridWeek, timeGridDay'
		},

		eventClick:function(info){

			mesStart=(info.event.start.getMonth()+1);
			diaStart=(info.event.start.getDate());
			anioStart=(info.event.start.getFullYear());

			mesEnd=(info.event.end.getMonth()+1);
			diaEnd=(info.event.end.getDate());
			anioEnd=(info.event.end.getFullYear());
			$('#id').val(info.event.id);
			$('#txtFecha').val(anioStart+"-"+mesStart+"-"+diaStart);
			$('#txtFechaFin').val(anioEnd+"-"+mesEnd+"-"+diaEnd);
			$('#txtTitulo').val(info.event.title);
			$('#txtDescripcion').val(info.event.extendedProps.descripcion);
			$('#exampleModal').modal('show');
		},

		events:"{{ url('/agenda/ver') }}"

	});
	calendar.setOption('locale', 'ES');	

	calendar.render();

	/*Cerrar modal*/
	botonCerrar = document.getElementById('btnClose');
	botonCerrar.addEventListener('click', cerrar, false);

	function cerrar(){
		$("#exampleModal").modal("hide");
	};	



});

</script>
@endsection

@section('content')
<h1>CALENDARIO CON TODOS LOS EVENTOS</h1>
<div class="container">	
	<div class="row">
		<div id="calendar"></div>
	</div>
</div>

<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Información del evento</h5>
				<button type="button" id="btnClose" class="close" data-toggle="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>	

			<div class="modal-body">
				<input type="hidden" id="_token" value="{{ csrf_token() }}">	
				<input type="hidden" name="id" id="id">				
				<label for="txtTitulo">Nombre del evento:</label>
				<input type="text" name="txtTitulo" id="txtTitulo" readonly class="form-control">
				<br>
				<label for="txtFecha">Fecha de inicio:</label>
				<input type="text" name="txtFecha" id="txtFecha" readonly class="form-control">
				<br>
				<label for="txtFechaFin">Fecha de fin:</label>
				<input type="text" name="txtFechaFin" id="txtFechaFin" readonly class="form-control">
				<br>					
					<!-- <label for="txtHora">Hora:</label>
					<input type="text" name="txtHora" id="txtHora" class="form-control">
					<br> -->
					<label for="txtDescripcion">Descripción:</label>
					<textarea name="txtDescripcion" id="txtDescripcion" cols="30" rows="10" class="form-control" readonly></textarea>
					<br>
					<!-- <label for="txtColor">Color:</label>
						<input type="color" name="txtColor" id="txtColor" class="form-control" class="form-control">	-->		
					</div>
					<div class="modal-footer">
					<!-- <button id="btnAgregar" class="btn btn-success">Añadir</button>
					<button id="btnModificar" class="btn btn-warning">Modificar</button>
					<button id="btnBorrar" class="btn btn-danger">Borrar</button>
					<button id="btnCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>	-->			
				</div> 		
			</div>			
		</div>
	</div>
	@endsection

