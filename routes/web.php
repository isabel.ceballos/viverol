<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventoController;
use App\Http\Controllers\FichaController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\AgendaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ContactanosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [EventoController::class, 'index'])->name('eventos.index');
Route::post('eventos/buscar', [EventoController::class, 'buscar']);
/*EVENTOS*/
Route::get('eventos', [EventoController::class, 'index'])->name('eventos.index');
Route::get('eventos/crear', [EventoController::class, 'create'])->name('eventos.create')->middleware('auth');
Route::get('eventos/{evento}', [EventoController::class, 'show'])->name('eventos.show');
Route::get('eventos/{evento}/editar', [EventoController::class, 'edit'])->name('eventos.edit')->middleware('auth');
Route::get('eventos/usuario/eventos', [EventoController::class, 'eventoUsuario'])->name('eventos.eventoUsuario')->middleware('auth');
Route::delete('eventos/{evento}', [EventoController::class, 'destroy'])->name('eventos.destroy');
Route::get('eventos/tipo/{tipo}', [EventoController::class, 'tipo'])->name('eventos.tipo');

/*CONTACTO*/
Route::get('contactanos', [ContactanosController::class, 'index'])->name('contactanos.index');
Route::post('contactanos', [ContactanosController::class, 'store'])->name('contactanos.store');

/*FICHAS*/
Route::get('fichas', [FichaController::class, 'index'])->name('fichas.index');
Route::get('fichas/crear', [FichaController::class, 'create'])->name('fichas.create')->middleware('auth');
Route::get('fichas/{ficha}', [FichaController::class, 'show'])->name('fichas.show');
Route::get('fichas/{ficha}/editar', [FichaController::class, 'edit'])->name('fichas.edit')->middleware('auth');
Route::delete('fichas/{ficha}', [FichaController::class, 'destroy'])->name('fichas.destroy');




Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/* POST */

Route::post('eventos', [EventoController::class, 'store'])->name('eventos.store');
Route::put('eventos/{evento}', [EventoController::class, 'update'])->name('eventos.update');
Route::post('eventos/{evento}/apuntar', [EventoController::class, 'apuntar'])->name('eventos.apuntar');
Route::post('eventos/{evento}/desapuntar', [EventoController::class, 'desApuntar'])->name('eventos.desapuntar');
Route::post('eventos/{evento}/comment', [CommentController::class, 'store'])->name('comments.store');


Route::post('fichas', [FichaController::class, 'store'])->name('fichas.store');
Route::put('ficha/{ficha}', [FichaController::class, 'update'])->name('fichas.update');

Route::put('user/profile/{user}', [UserController::class, 'updateProfile'])->name('users.update');

/*AGENDA*/

Route::get('agenda', [EventoController::class, 'agenda'])->name('agendas.index');
//Route::post('agenda', [EventoController::class, 'store'])->name('eventos.store');
Route::get('agenda/ver', [EventoController::class, 'ver']);


/*ADMIN*/
Route::get('user/admin', [HomeController::class, 'index'])->name('admin.index');


Route::get('descargar-usuarios/{evento}', [EventoController::class, 'pdf'])->name('usuarios.pdf');
