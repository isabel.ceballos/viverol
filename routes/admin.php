<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;

Route::get('/', [HomeController::class, 'index'])->middleware('can:admin.index')->name('admin.index');

Route::get('tipos', [HomeController::class, 'showTipos'])->middleware('can:admin.tipos')->name('admin.tipos');
Route::delete('tipos/{tipo}', [HomeController::class, 'destroyTipo'])->name('tipos.destroy');
Route::get('tipos/{tipo}/editar', [HomeController::class, 'editTipo'])->name('admin.editTipo');

Route::get('tipos/crear', [HomeController::class, 'createTipo'])->name('tipos.create');

Route::put('tipos/{tipo}', [HomeController::class, 'updateTipo'])->name('tipos.update');
Route::post('tipos', [HomeController::class, 'storeTipo'])->name('tipos.store');

Route::resource('users', UserController::class)->only(['index', 'edit', 'update'])->names('admin.users');
Route::delete('users/{user}', [HomeController::class, 'destroyUser'])->name('users.destroy');

Route::get('comments', [HomeController::class, 'showComments'])->name('admin.comments');
Route::delete('comments/{comment}', [HomeController::class, 'destroyComment'])->name('comments.destroy');

Route::get('eventos', [HomeController::class, 'showEventos'])->name('admin.eventos');
Route::delete('eventos/{evento}', [HomeController::class, 'destroyEvento'])->name('eventos.destroyEvento');