<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1= Role::create(['name' =>'Admin']);
        $role2= Role::create(['name' =>'User']);

        Permission::create(['name'=>'admin.index'])->assignRole($role1);

        Permission::create(['name'=>'admin.tipos'])->assignRole($role1);
        Permission::create(['name'=>'tipos.create'])->assignRole($role1);
        Permission::create(['name'=>'tipos.edit'])->assignRole($role1);
        Permission::create(['name'=>'tipos.destroy'])->assignRole($role1);
        Permission::create(['name'=>'tipos.store'])->assignRole($role1);

        Permission::create(['name'=>'admin.users.index'])->assignRole($role1);
        Permission::create(['name'=>'admin.users.edit'])->assignRole($role1);
        Permission::create(['name'=>'admin.users.update'])->assignRole($role1);

        Permission::create(['name'=>'admin.comments'])->assignRole($role1);
        Permission::create(['name'=>'comments.destroy'])->assignRole($role1);

        Permission::create(['name'=>'admin.eventos'])->assignRole($role1);
        Permission::create(['name'=>'eventos.destroy'])->assignRole($role1);

    }
}
