<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User; 
use App\Models\Evento;
use App\Models\Tipo_Evento;
use Illuminate\Support\Str;
class EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$e= new Evento();
        $e->user_id=User::first()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Old Carucedo';
        $e->descripcion='Evento de spaghetti western.';
        $e->imagen='oldcarucedo.jpg';
        $e->color='#df923a';
        //$e->textColor='';
        $e->start='2021-01-11';
        $e->end='2021-01-15';
        $e->slug=Str::slug($e->title);
        $e->save();
       
        $e= new Evento();
        $e->user_id=User::first()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Dragones y Mazmorras';
        $e->descripcion='Evento de dragones y mazmorras.';
        $e->imagen='dragonesymazmorras.jpg';
        $e->color='#df923a';
        //$e->textColor='';
        $e->start='2020-01-11';
        $e->end='2020-01-15';
        $e->slug=Str::slug($e->title);
        $e->save();

        $e= new Evento();
        $e->user_id=User::first()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Anima';
        $e->descripcion='Evento del juego de rol Anima.';
        $e->imagen='anima.jpg';
        $e->color='#b3e6ff';
        //$e->textColor='';
        $e->start='2021-04-13';
        $e->end='2021-04-15';
        $e->slug=Str::slug($e->title);
        $e->save();
       

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Buffy';
        $e->descripcion='Evento de buffy.';
        $e->imagen='buffy.jpg';
        $e->color='#dd99ff';
        //$e->textColor='';
        $e->start='2021-04-25';
        $e->end='2021-04-30';
        $e->slug=Str::slug($e->title);
        $e->save();
       

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='The call of Cthulhu';
        $e->descripcion='Evento de call of Cthulhu.';
        $e->imagen='cthulhu.jpg';
        $e->color='#80ffd4';
        //$e->textColor='';
        $e->start='2021-04-30';
        $e->end='2021-05-05';
        $e->slug=Str::slug($e->title);
        $e->save();
       

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Pirata';
        $e->descripcion='Evento de piratas.';
        $e->imagen='pirata.jpg';
        $e->color='#99ff99';
        //$e->textColor='';
        $e->start='2021-04-15';
        $e->end='2021-04-17';
        $e->slug=Str::slug($e->title);
        $e->save();
        

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='El señor de los anillos';
        $e->descripcion='Evento de el señor de los anillos.';
        $e->imagen='rings.jpg';
        $e->color='#ffe680';
        //$e->textColor='';
        $e->start='2021-04-15';
        $e->end='2021-04-17';
        $e->slug=Str::slug($e->title);
        $e->save();

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='Star Wars';
        $e->descripcion='Evento de Star wars.';
        $e->imagen='starwars.jpg';
        $e->color='#4da6ff';
        //$e->textColor='';
        $e->start='2021-04-15';
        $e->end='2021-04-17';
        $e->slug=Str::slug($e->title);
        $e->save();

        $e= new Evento();
        $e->user_id=User::all()->random()->id;
        $e->tipo_evento_id=Tipo_Evento::all()->random()->id;
        $e->title='World of Warcraft';
        $e->descripcion='Evento de World of Warcraft.';
        $e->imagen='wow.jpg';
        $e->color='#ff4d4d';
        //$e->textColor='';
        $e->start='2021-04-15';
        $e->end='2021-04-17';
        $e->slug=Str::slug($e->title);
        $e->save();
       
       Evento::factory(40)->create();

        $eventos = Evento::all();
        foreach($eventos as $evento)
        {
            $evento->usuariosApuntados()->attach([
                rand(1,5),
                rand(6,10)
            ]);
        }

        
    }
}
