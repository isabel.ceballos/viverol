<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        /*DB::table('eventos')->delete();
        DB::table('users')->delete();*/
        //$this->call(UserSeeder::class);
       // DB::table('tipo_eventos')->delete();
       // $this->call(Tipo_EventoSeeder::class);       
        //\App\Models\User::factory(10)->create();
       
        //$this->call(EventoSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(Tipo_EventoSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FichaSeeder::class);
        $this->call(EventoSeeder::class);
        $this->call(CommentSeeder::class);
    }
}

 