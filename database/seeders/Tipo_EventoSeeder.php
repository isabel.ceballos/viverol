<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tipo_Evento;
class Tipo_EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t= new Tipo_Evento();
        $t->tipo = "Rol en vivo";
        $t->save();

        $t2= new Tipo_Evento();
        $t2->tipo = "Rol de mesa";
        $t2->save();

        $t2= new Tipo_Evento();
        $t2->tipo = "Rol";
        $t2->save();

        $t2= new Tipo_Evento();
        $t2->tipo = "Rol online";
        $t2->save();
    }
}
