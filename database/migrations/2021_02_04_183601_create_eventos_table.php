<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('tipo_evento_id');
            $table->foreign('tipo_evento_id')->references('id')->on('tipo_eventos');
            $table->string('title', 255);
            $table->text('descripcion');
            $table->string('imagen', 50)->nullable();
            
            $table->string('color', 20)->nullable();
            $table->string('textColor', 20)->default('#000000');

            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('slug')->unique();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
