<?php

namespace Database\Factories;

use App\Models\Ficha;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class FichaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ficha::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'nombre_personaje' => $this->faker->name,
            'tipo_personaje' => $this->faker->word,
            'descripcion' => $this->faker->paragraph,
            'imagen' => 'tauriel.jpg',
        ];
    }
}
