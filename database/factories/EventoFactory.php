<?php

namespace Database\Factories;

use App\Models\Evento;
use App\Models\User;
use App\Models\Tipo_Evento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EventoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Evento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title=$this->faker->sentence($nbWords = 3, $variableNbWords = true);
        $start=$this->faker->dateTimeThisYear($max = '2021-12-31 22:30:48', $timezone = null);
        return [
            'title' => $title,
            'descripcion' => $this->faker->paragraph,
            'start' => $start,
            'end' => $this->faker->dateTimeBetween($startDate = $start, $endDate = '2021-12-31 22:30:48', $timezone = null),
            'user_id' => User::all()->random()->id,
            'tipo_evento_id' => Tipo_Evento::all()->random()->id,
            'imagen' => 'evento_por_defecto.jpg',
            'color' => $this->faker->hexcolor,
            'slug' => Str::slug($title)
        ];
    }
}
